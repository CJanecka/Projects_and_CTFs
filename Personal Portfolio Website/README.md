# Personal Portfolio Website

![portfolio2](https://github.com/CJanecka/Projects_and_CTFs/assets/131223318/08f0615e-937c-4485-ab04-ed641aa04fd6)

## Table of Contents

  + [01. Overview](#Overview)
  + [02. Equipment and Tools](#Equipment-and-Tools)
  + [03. Digital Portfolio](#Digital-Portfolio)
    - [a. Home Page](#Home-Page)
    - [b. About Me Page](#About-Me-Page)
    - [c. Portfolio Page](#Portfolio-Page)
    - [d. Blog Page](#Blog-Page)
    - [d. Contact Page](#Contact-Page)

## Overview

*This page is incomplete, I am updating this while studying for the Sec+ certification exam. Some of the information included may only serve as a placeholder, or template, for later revision. I appreciate your patience while I complete this page.*

I based my design largely on a tutorial from [freeCodeCamp on YouTube](https://www.youtube.com/watch?v=xV7S8BhIeBo). While working through the tutorial, I faced some coding issues that affected the functionality of certain elements on the webpage. To address these issues, and tailor the page design to my preferences, I made additional modifications to improve both aesthetics and functionality. 

This repository displays a redacted version of the portfolio I created. I am currently reviewing and refining the code. Upon completion, my intention is to upload the full code to GitHub, allowing anyone interested to effortlessly replicate the page.

<cont here>

## Equipment and Tools

  + [Visual Studio Code](https://code.visualstudio.com/docs/supporting/faq)
    - Extensions Enabled:
      + [HTML CSS Support](https://marketplace.visualstudio.com/items?itemName=ecmel.vscode-html-css)
      + [HTML Snippets](https://marketplace.visualstudio.com/items?itemName=abusaidm.html-snippets)
      + [Live Sass Compiler](https://marketplace.visualstudio.com/items?itemName=ritwickdey.live-sass)
      + [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)
      + [Sass (.sass only)](https://marketplace.visualstudio.com/items?itemName=Syler.sass-indented)
  + Programming Languages:
    - [HyperText Markup Language](https://www.geeksforgeeks.org/what-is-html/) (HTML)
    - [Cascading Style Sheets](https://www.geeksforgeeks.org/types-of-css-cascading-style-sheet/) (CSS)
    - [JavaScript](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/What_is_JavaScript) (JS)
    - [Sassy Cascading Style Sheets](https://www.geeksforgeeks.org/what-is-the-difference-between-css-and-scss/) (SCSS)
