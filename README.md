# Projects and CTFs

These are some of the projects and capture-the-flag challenges I've completed, both during a Cybersecurity Bootcamp and on my own. I found these experiences particularly enjoyable. My goal is to share the knowledge I've gained and encourage collaboration as I continue to grow in the field. 

Please let me know if you notice any errors or feel free to reach out if you're interested in connecting.

![chrome_WkRjV4I5H5](https://github.com/CJanecka/Projects_and_CTFs/assets/131223318/2c221367-ad43-43ff-a572-7e166f19c376)
